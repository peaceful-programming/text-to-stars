import os
import sys
import json
import time
import shutil
import pickle
import logging
import data_helper
import numpy as np
import tensorflow as tf
from cnn_rnn import TextCNNRNN

logging.getLogger().setLevel(logging.INFO)


def train_cnn_rnn():

    training_config = sys.argv[1]
    params = json.loads(open(training_config).read())

    # Assign a 300 dimension vector to each word
    name = 'B00FRHTSK4'
    pickle_files, tfrecords_files = data_helper.load_data(name)

    embedding_mat = pickle_files['embedding_mat']

    # x_train, y_train = tfrecords_files['train'].get_next()
    # x_dev, y_dev = tfrecords_files['dev'].get_next()
    # x_test, y_test = tfrecords_files['test'].get_next()

    train_dataset = tfrecords_files['train']
    dev_dataset = tfrecords_files['dev']
    test_dataset = tfrecords_files['test']

    # logging.info('x_train: {}, x_dev: {}, x_test: {}'.format(len(x_train), len(x_dev), len(x_test)))
    # logging.info('y_train: {}, y_dev: {}, y_test: {}'.format(len(y_train), len(y_dev), len(y_test)))

    # Create a directory, everything related to the training will be saved in this directory
    timestamp = str(int(time.time()))
    trained_dir = './trained_results_' + timestamp + '/'
    if os.path.exists(trained_dir):
        shutil.rmtree(trained_dir)
    os.makedirs(trained_dir)

    graph = tf.Graph()
    with graph.as_default():
        session_conf = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
        sess = tf.Session(config=session_conf)
        with sess.as_default():
            cnn_rnn = TextCNNRNN(
                embedding_mat=embedding_mat,
                # sequence_length=x_train.shape[1],
                # num_classes=y_train.shape[1],
                sequence_length=300,
                num_classes=5,
                non_static=params['non_static'],
                hidden_unit=params['hidden_unit'],
                max_pool_size=params['max_pool_size'],
                filter_sizes=map(int, params['filter_sizes'].split(",")),
                num_filters=params['num_filters'],
                embedding_size=params['embedding_dim'],
                l2_reg_lambda=params['l2_reg_lambda'])

            global_step = tf.Variable(0, name='global_step', trainable=False)
            optimizer = tf.train.RMSPropOptimizer(1e-3, decay=0.9)
            grads_and_vars = optimizer.compute_gradients(cnn_rnn.loss)
            train_op = optimizer.apply_gradients(grads_and_vars, global_step=global_step)

            # Checkpoint files will be saved in this directory during training
            checkpoint_dir = './checkpoints_' + timestamp + '/'
            if os.path.exists(checkpoint_dir):
                shutil.rmtree(checkpoint_dir)
            os.makedirs(checkpoint_dir)
            checkpoint_prefix = os.path.join(checkpoint_dir, 'model')

            def real_len(batches):
                # return [np.ceil(np.argmin(batch + [0]) * 1.0 / params['max_pool_size']) for batch in batches]
                return 300

            def train_step(x_batch, y_batch):
                feed_dict = {
                    cnn_rnn.input_x: sess.run(x_batch),
                    cnn_rnn.input_y: sess.run(y_batch),
                    cnn_rnn.dropout_keep_prob: params['dropout_keep_prob'],
                    cnn_rnn.batch_size: params['batch_size'],
                    cnn_rnn.pad: np.zeros([params['batch_size'], 1, params['embedding_dim'], 1]),
                    cnn_rnn.real_len: real_len(x_batch)
                }
                import pdb; pdb.set_trace()
                _, step, loss, accuracy = sess.run([train_op, global_step, cnn_rnn.loss, cnn_rnn.accuracy], feed_dict)

            def dev_step(x_batch, y_batch):
                feed_dict = {
                    cnn_rnn.input_x: sess.run(x_batch),
                    cnn_rnn.input_y: sess.run(y_batch),
                    cnn_rnn.dropout_keep_prob: 1.0,
                    cnn_rnn.batch_size: params['batch_size'],
                    cnn_rnn.pad: np.zeros([params['batch_size'], 1, params['embedding_dim'], 1]),
                    cnn_rnn.real_len: real_len(x_batch)
                }
                step, loss, accuracy, num_correct, predictions = sess.run(
                    [global_step, cnn_rnn.loss, cnn_rnn.accuracy, cnn_rnn.num_correct, cnn_rnn.predictions], feed_dict)
                return accuracy, loss, num_correct, predictions

            saver = tf.train.Saver()
            sess.run(tf.global_variables_initializer())

            # Training starts here
            # train_batches = data_helper.batch_iter(list(zip(x_train, y_train)), params['batch_size'], params['num_epochs'])
            # x_train_batch, y_train_batch = train_dataset.batch(params['batch_size']).make_one_shot_iterator().get_next()
            x_train_batch, y_train_batch = train_dataset.make_one_shot_iterator().get_next()
            # x_train_batch, y_train_batch = train_dataset.batch(params['batch_size']).get_next()
            best_dev_accuracy, best_at_step = 0, 0

            # Train the model with x_train and y_train
            while True:
                try:
                    # for train_batch in train_batches:
                        # x_train_batch, y_train_batch = zip(*train_batch)
                    train_step(x_train_batch, y_train_batch)
                    current_step = tf.train.global_step(sess, global_step)
                    if current_step % params['evaluate_every'] == 0:
                        # x_dev_batch, y_dev_batch = dev_dataset.batch(params['batch_size']).make_one_shot_iterator().get_next()
                        x_dev_batch, y_dev_batch = dev_dataset.make_one_shot_iterator().get_next()
                        # dev_batches = data_helper.batch_iter(list(zip(x_dev, y_dev)), params['batch_size'], 1)
                        # total_dev_correct = 0
                        dev_accuracies = []
                        while True:
                            try:
                                # for dev_batch in dev_batches:
                                    # x_dev_batch, y_dev_batch = zip(*dev_batch)
                                acc, loss, num_dev_correct, predictions = dev_step(x_dev_batch, y_dev_batch)
                                # total_dev_correct += num_dev_correct
                                dev_accuracies += [acc]
                            except tf.errors.OutOfRangeError:
                                break
                        # accuracy = float(total_dev_correct) / len(y_dev)
                        dev_accuracy = np.mean(dev_accuracies)
                        logging.info('Accuracy on dev set: {}'.format(dev_accuracy))

                        if dev_accuracy >= best_dev_accuracy:
                            best_dev_accuracy, best_at_step = dev_accuracy, current_step
                            path = saver.save(sess, checkpoint_prefix, global_step=current_step)
                            logging.critical('Saved model {} at step {}'.format(path, best_at_step))
                            logging.critical('Best accuracy {} at step {}'.format(best_dev_accuracy, best_at_step))
                except tf.errors.OutOfRangeError:
                    break

                # Evaluate the model with x_dev and y_dev
            logging.critical('Training is complete, testing the best model on x_test and y_test')

            # Save the model files to trained_dir. predict.py needs trained model files.
            saver.save(sess, trained_dir + "best_model.ckpt")

            # Evaluate x_test and y_test
            saver.restore(sess, checkpoint_prefix + '-' + str(best_at_step))
            # x_test_batch, y_test_batch = test_dataset.batch(params['batch_size']).make_one_shot_iterator().get_next()
            x_test_batch, y_test_batch = test_dataset.make_one_shot_iterator().get_next()
            # test_batches = data_helper.batch_iter(list(zip(x_test, y_test)), params['batch_size'], 1, shuffle=False)
            # total_test_correct = 0
            test_accuracies = []
            while True:
                try:
                    # for test_batch in test_batches:
                    #     x_test_batch, y_test_batch = zip(*test_batch)
                    acc, loss, num_test_correct, predictions = dev_step(x_test_batch, y_test_batch)
                    # total_test_correct += int(num_test_correct)
                    test_accuracies += [acc]
                    test_accuracy = np.mean(test_accuracies)
                    logging.critical('Accuracy on test set: {}'.format(test_accuracy))
                except tf.errors.OutOfRangeError:
                    break

    # Save trained parameters and files since predict.py needs them
    with open(trained_dir + 'embeddings.pickle', 'wb') as outfile:
        pickle.dump(embedding_mat, outfile, pickle.HIGHEST_PROTOCOL)

    # params['sequence_length'] = x_train.shape[1]
    params['sequence_length'] = 300
    with open(trained_dir + 'trained_parameters.json', 'w') as outfile:
        json.dump(params, outfile, indent=4, sort_keys=True, ensure_ascii=False)


if __name__ == '__main__':
    # python3 train.py ./data/train.csv.zip ./training_config.json
    train_cnn_rnn()
