import os
import pickle
import tensorflow as tf

pickles_path = os.path.join(os.getcwd(), '../tfrecord-data/pickles')
tfrecords_path = os.path.join(os.getcwd(), '../tfrecord-data/tfrecords')


feature_set = {'text': tf.FixedLenFeature([], tf.string),
               'cleaned_text': tf.FixedLenFeature([], tf.string),
               'ints': tf.VarLenFeature(tf.int64),
               'stars': tf.FixedLenFeature([5], tf.int64)}


def load_pickle_file(short_filename):
    return pickle.load(open(os.path.join(pickles_path, short_filename + ".p"), "rb"))


def load_pickle_files():
    word_embedding_matrix = load_pickle_file('word_embedding_matrix')
    int_to_vocab = load_pickle_file('int_to_vocab')
    vocab_to_int = load_pickle_file('vocab_to_int')
    return {
        'embedding_mat': word_embedding_matrix,
        'vocab_to_int': vocab_to_int,
        'int_to_vocab': int_to_vocab
    }


def _parse_function(example_proto):
    parsed_features = tf.parse_single_example(example_proto, feature_set)
    return parsed_features["ints"].values, parsed_features["stars"]


def load_tfrecords_file(short_filename):
    filename = os.path.join(tfrecords_path, short_filename + '.tfrecords')
    dataset = tf.data.TFRecordDataset([filename])
    # dataset = dataset.shuffle(buffer_size=10000)
    dataset = dataset.map(_parse_function)
    # dataset = dataset.make_one_shot_iterator()
    # x, y = iterator.get_next()
    return dataset


def load_tfrecords_files(name):
    train = load_tfrecords_file(name + '_train')
    dev = load_tfrecords_file(name + '_dev')
    test = load_tfrecords_file(name + '_test')
    return {
        'train': train,
        'dev': dev,
        'test': test
    }


def load_data(name):
    pickle_files = load_pickle_files()
    tfrecords_files = load_tfrecords_files(name)
    return pickle_files, tfrecords_files
