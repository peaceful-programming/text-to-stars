"""
Greatly influenced by https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/how_tos/reading_data/convert_to_records.py
Converts scraped data to TFRecords file format with Example protos using word embeddings.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import jsonlines
import numpy as np
import os
import sys
import tensorflow as tf

from clean_text import clean_text
from word_embedder import WordEmbedder

FLAGS = None


def _int64_feature(value, is_list=False):
    if not is_list:
        value = [value]
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def _bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def create_sources(name):
    train_amount = int((100 - FLAGS.dev_test_percent) / FLAGS.dev_test_percent)
    source = os.path.join(FLAGS.source_directory, name + '.jl')
    train_source = os.path.join(FLAGS.source_directory, name + '_train.jl')
    dev_source = os.path.join(FLAGS.source_directory, name + '_dev.jl')
    test_source = os.path.join(FLAGS.source_directory, name + '_test.jl')
    with jsonlines.open(source) as f_source, jsonlines.open(train_source, mode='w') as f_train, jsonlines.open(dev_source, mode='w') as f_dev, jsonlines.open(test_source, mode='w') as f_test:
        counter = 0
        for item in f_source:
            if counter < train_amount:
                f_train.write(item)
            if counter == train_amount:
                f_dev.write(item)
            if counter > train_amount:
                f_test.write(item)
                counter = -1
            counter += 1
    return train_source, dev_source, test_source


def convert_jl_to_tfrecords(we: WordEmbedder, source, target):
    longest_ints_length = 0
    with tf.python_io.TFRecordWriter(target) as writer:
        with jsonlines.open(source) as f:
            for item in f:
                cleaned_text = clean_text(item['text'], remove_stopwords=False)
                ints, _, _ = we.convert_to_ints(cleaned_text)
                longest_ints_length = max(len(ints), longest_ints_length)
                stars = np.identity(5, dtype=np.int64)[item['stars'] - 1]
                example = tf.train.Example(
                    features=tf.train.Features(
                        feature={
                            'text': _bytes_feature(tf.compat.as_bytes(item['text'])),
                            'ints': _int64_feature(ints, is_list=True),
                            'cleaned_text': _bytes_feature(tf.compat.as_bytes(cleaned_text)),
                            'stars': _int64_feature(stars, is_list=True)
                        }))
                writer.write(example.SerializeToString())
    print("longest int length is {}".format(longest_ints_length))


def main(unused_argv):
    name = 'B00FRHTSK4'
    train_source, dev_source, test_source = create_sources(name)
    train_target = os.path.join(FLAGS.target_directory, name + '_train.tfrecords')
    dev_target = os.path.join(FLAGS.target_directory, name + '_dev.tfrecords')
    test_target = os.path.join(FLAGS.target_directory, name + '_test.tfrecords')

    we = WordEmbedder(train_source)
    convert_jl_to_tfrecords(we, train_source, train_target)
    convert_jl_to_tfrecords(we, dev_source, dev_target)
    convert_jl_to_tfrecords(we, test_source, test_target)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--source_directory',
        type=str,
        default=os.path.join(os.getcwd(), '../scrape-data/harvest'),
        help='Directory to data files'
     )
    parser.add_argument(
        '--target_directory',
        type=str,
        default=os.path.join(os.getcwd(), 'tfrecords'),
        help='Directory to output tfrecords files'
     )
    parser.add_argument(
        '--dev_test_percent',
        type=int,
        default=10,
        help='Percent of data which is split to make dev/test sets'
     )
    FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
