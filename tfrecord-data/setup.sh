mkdir word_embeddings
mkdir pickles
mkdir tfrecords
cd word_embeddings
wget https://conceptnet.s3.amazonaws.com/downloads/2017/numberbatch/numberbatch-en-17.06.txt.gz
gunzip -k numberbatch-en-17.06.txt.gz
cd ..
