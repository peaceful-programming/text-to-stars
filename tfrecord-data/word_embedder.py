import jsonlines
import numpy as np
import pickle

from clean_text import clean_text

"""
Greatly influenced by https://github.com/Currie32/Text-Summarization-with-Amazon-Reviews/blob/master/summarize_reviews.py
"""


class WordEmbedder():
    word_count_dict = {}
    missing_words = []
    vocab_to_int = {}  # Dictionary to convert words to integers
    int_to_vocab = {}  # Dictionary to convert integers to words
    embedding_dim = -1  # size of the embeddings vector (to be inferred)
    word_embedding_matrix = None

    def __init__(self, source_filename, missing_word_threshold=20, embeddings_file='word_embeddings/numberbatch-en-17.06.txt'):
        """Constructor for WordEmbedder

        Args:
            source_filename (string): The filepath of the file containing the scraped
                lines of text.
            missing_word_threshold (int): The min number of times a word must appear
                in the texts in order to be considered a "missing word" if it is not
                found in the given embedding file. Defaults to 20.
            embeddings_file (string, optional): The filepath of the embeddings file
                to use. Defaults to 'word_embeddings/numberbatch-en-17.06.txt'.

        Returns:
        WordEmbedder: a class to handle word embeddings

        """
        self.embeddings_index = {}
        self.missing_word_threshold = missing_word_threshold
        with open(embeddings_file, encoding='utf-8') as f:
            for line in f:
                values = line.split(' ')
                word = values[0]
                embedding = np.asarray(values[1:], dtype='float32')
                self.embeddings_index[word] = embedding
                if len(values) - 1 > self.embedding_dim:
                    self.embedding_dim = len(values) - 1
        with jsonlines.open(source_filename) as f:
            for item in f:
                cleaned_text = clean_text(item['text'], remove_stopwords=False)
                self.count_words(cleaned_text)
        self.find_missing_words()
        self.create_vocab_to_int()
        self.create_word_embeddings()

    def count_words(self, cleaned_text):
        '''Count the number of occurrences of each word in a cleaned_text'''
        for word in cleaned_text.split():
            if word not in self.word_count_dict:
                self.word_count_dict[word] = 1
            else:
                self.word_count_dict[word] += 1

    def find_missing_words(self):
        """ Find number of words missing from embeddings file, and used more than the threshold."""
        for word, count in self.word_count_dict.items():
            if count > self.missing_word_threshold:
                if word not in self.embeddings_index:
                    self.missing_words += [word]
        missing_ratio = round(len(self.missing_words)/len(self.word_count_dict), 4)*100
        print("Words missing from CN:", self.missing_words)
        print("Percent of words that are missing from vocabulary: {}%".format(missing_ratio))

    def create_vocab_to_int(self):
        value = 0
        for word, count in self.word_count_dict.items():
            if count > self.missing_word_threshold or word in self.embeddings_index:
                self.vocab_to_int[word] = value
                value += 1

        # Special tokens that will be added to our vocab
        codes = ["<UNK>", "<PAD>", "<EOS>", "<GO>"]

        # Add codes to vocab
        for code in codes:
            self.vocab_to_int[code] = len(self.vocab_to_int)

        for word, value in self.vocab_to_int.items():
            self.int_to_vocab[value] = word

        filepath_vocab_to_int = 'pickles/vocab_to_int.p'
        filepath_int_to_vocab = 'pickles/int_to_vocab.p'
        pickle.dump(self.vocab_to_int, open(filepath_vocab_to_int, 'wb'))
        pickle.dump(self.int_to_vocab, open(filepath_int_to_vocab, 'wb'))
        usage_ratio = round(len(self.vocab_to_int) / len(self.word_count_dict), 4)*100
        print("Total number of unique words:", len(self.word_count_dict))
        print("Number of words we will use:", len(self.vocab_to_int))
        print("Percent of words we will use: {}%".format(usage_ratio))

    def create_word_embeddings(self):
        print("embedding dim: {}".format(self.embedding_dim))
        nb_words = len(self.vocab_to_int)

        # Create matrix with default values of zero
        self.word_embedding_matrix = np.zeros((nb_words, self.embedding_dim), dtype=np.float32)
        for word, i in self.vocab_to_int.items():
            if word in self.embeddings_index:
                self.word_embedding_matrix[i] = self.embeddings_index[word]
            else:
                # If word not in CN, create a random embedding for it
                new_embedding = np.array(np.random.uniform(-1.0, 1.0, self.embedding_dim))
                self.embeddings_index[word] = new_embedding
                self.word_embedding_matrix[i] = new_embedding

        # Check if value matches len(vocab_to_int)
        if len(self.word_embedding_matrix) == len(self.vocab_to_int):
            filepath = 'pickles/word_embedding_matrix.p'
            pickle.dump(self.word_embedding_matrix, open(filepath, 'wb'))
            print("Word embedding matrix created successfully")
            print("Saved as {}".format(filepath))
        else:
            print("Problem creating the word embedding matrix")

    def convert_to_ints(self, cleaned_text, word_count=0, unk_count=0, eos=False):
        '''Convert words in cleaned_text to an integer.
           If word is not in vocab_to_int, use UNK's integer.
           Total the number of words and UNKs.
           Add EOS token to the end of texts'''
        ints = np.array([], dtype=np.int64)
        for word in cleaned_text.split():
            word_count += 1
            if word in self.vocab_to_int:
                ints = np.append(ints, self.vocab_to_int[word])
            else:
                ints = np.append(ints, self.vocab_to_int["<UNK>"])
                unk_count += 1
        if eos:
            ints = np.append(np, self.vocab_to_int["<EOS>"])
        return ints, word_count, unk_count
