import glob
import tensorflow as tf


feature_set = {'text': tf.FixedLenFeature([], tf.string),
               'cleaned_text': tf.FixedLenFeature([], tf.string),
               'ints': tf.VarLenFeature(tf.int64),
               'stars': tf.FixedLenFeature([5], tf.int64)}


def reader_way():
    reader = tf.TFRecordReader()
    filenames = glob.glob('tfrecords/*.tfrecords')
    filename_queue = tf.train.string_input_producer(filenames)
    _, serialized_example = reader.read(filename_queue)
    features = tf.parse_single_example(serialized_example, features=feature_set)
    stars = features['stars']
    text = features['text']
    init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    with tf.Session() as sess:
        sess.run(init_op)
        coord = tf.train.Coordinator()
        tf.train.start_queue_runners(sess=sess, coord=coord)
        print(sess.run([text, stars]))


def _parse_function(example_proto):
    parsed_features = tf.parse_single_example(example_proto, feature_set)
    return {'stars': parsed_features["stars"],
            'cleaned_text': parsed_features["cleaned_text"],
            'ints': parsed_features["ints"],
            'text': parsed_features["text"]}


def record_way():
    """https://www.tensorflow.org/programmers_guide/datasets """
    filenames = glob.glob('tfrecords/*.tfrecords')
    dataset = tf.data.TFRecordDataset(filenames)
    # dataset = dataset.shuffle(buffer_size=10000)
    dataset = dataset.map(_parse_function)
    iterator = dataset.make_one_shot_iterator()
    next_element = iterator.get_next()
    stars = next_element['stars']
    text = next_element['text']
    ints = next_element['ints']
    cleaned_text = next_element['cleaned_text']
    init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    with tf.Session() as sess:
        sess.run(init_op)
        [cleaned_text, text, stars, ints] = sess.run([cleaned_text, text, stars, ints])
        text = text.decode()
        cleaned_text = cleaned_text.decode()
        import pdb; pdb.set_trace()
        print(ints, '\n', cleaned_text, '\n', text, '\n', stars)


def main():
    # reader_way()
    record_way()


if __name__ == "__main__":
    main()
