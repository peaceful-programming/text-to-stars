### Setup

##### FIRST - pip installation
I (always) highly recommend doing this is a virtual environment
```
pip install -r dev-requirements.txt
pip install -r requirements.txt
```

##### SECOND - getting the word embeddings
using the anglocentric English-only 17.06 version of https://github.com/commonsense/conceptnet-numberbatch
Just run setup.sh:
```
chmod +x setup.sh
./setup.sh
```

##### THIRD - installing installing nltk stopwords
This is easy to do but is a little subtle if you use a virtual environment.
On my system, I use pyenv + virtualenv.
The pyenv for this project is called `tfrecords`.
So I do this:
Note that you will have to replace the tilda symbol `~` with the actual path to your home folder.
```
$ python
>>> import nltk
>>> nltk.download('stopwords', download_dir='~/.pyenv/versions/tfrecords/lib/nltk_data')
```
I do this so that my system is not clogged with nltk resources. Different projects use different nltk resources.

### Running the convesion:
```
python convert_scrape_harvest_to_tfrecords.py 

```
This will create a pickle files and tfrecords files

