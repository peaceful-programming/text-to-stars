# text-to-stars project

This project is actually several individual projects, all of which build on each other.
They should be taken in this order:

### 1. scrape-data
Scrapes amazon reviews and outputs to a JSON lines (jl) file

### 2. tfrecord-data
Turns jl files into tfrecord files and creates pickled word_embeddings

### 3. cnn_rnn
Does the ML magic
