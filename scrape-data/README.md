This project with commands like:
$ scrapy startproject amazon
$ scrapy genspider sata_drive amazon.com

### Run the sata drive spider
$ scrapy crawl satadrive -o harvest/satadrive.jl

### Inheriting the ProductSpider
I made a generic ProductSpider which can be inherited. See the SeagateDriveSpider for an illustration. Note that the the name must be given for scrapy to find it. This name has to be the product code since it is used to build the URL.


