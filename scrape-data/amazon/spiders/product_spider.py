# -*- coding: utf-8 -*-
import scrapy


class ProductSpider(scrapy.Spider):
    allowed_domains = ['amazon.com']

    def __init__(self, product_code):
        self.name = product_code
        self.start_urls = ['https://www.amazon.com/gp/product/{0}'.format(product_code)]

    def parse(self, response):
        yield response.follow(response.css('#acrCustomerReviewLink::attr(href)').extract_first(), callback=self.parse_reviews_link)

    def parse_reviews_link(self, rl):
        href = rl.css('#reviews-medley-footer div a::attr(href)').extract_first()
        yield rl.follow(href, callback=self.parse_all_reviews_link)

    def parse_all_reviews_link(self, rl):
        for review in rl.css('.a-section .review'):
            stars = review.css('.a-icon-star .a-icon-alt::text').extract_first()
            text = []
            text += review.css('.review-text::text').extract()
            text += review.css('.review-text div::text').extract()
            text += review.css('.review-text div div::text').extract()
            if len(text) != 0:
                yield {
                    'text': '\n'.join(text),
                    'stars': self.stars_int_from_string(stars)
                   }
        href = rl.css('.a-pagination .a-last a::attr(href)').extract_first()
        yield rl.follow(href, callback=self.parse_all_reviews_link)

    def stars_int_from_string(self, stars_string):
        for i in range(1, 6):
            if stars_string.find(str(i)) > -1:
                return i
        return None
